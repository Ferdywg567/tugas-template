<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model {

    private $_db;
    
    function __construct() {
        parent::__construct();
    }
    
    public function SetDb($db) {
        $this->_db = $db;
        $this->_db->conn_id->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
    }

    public function get_laporan() {
        $sql = "SELECT * FROM sentitems order by SendingDateTime, DestinationNumber DESC LIMIT 10";
        
        $sth = $this->_db->conn_id->prepare($sql);
        $sth->execute();
        
        $all_array = $sth->fetchAll();
        
        return $all_array;
    }
    function fetch_data($query){
        $this->db->select("*");
        $this->db->from('sentitems');
        if ($query != '') {
            $this->db->like('ID', $query);
            $this->db->or_like('TextDecoded', $query);
            $this->db->or_like('DestinationNumber', $query);
        }
        $this->db->order_by('ID', 'DESC');
        return $this->db->get();
    }
}
