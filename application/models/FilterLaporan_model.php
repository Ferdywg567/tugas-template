<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Filterlaporan_model extends CI_Model {

    private $all_array = array();
    public $_dbdefault;

    function __construct(){
        $this->_dbdefault = array(
            'dbdriver' => 'pdo',
            'dbprefix' => '',
            'pconnect' => TRUE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );
    }
    public function get_filter_keyword($keyword, $keyword2, $keyword3, $keyword4, $locate)
    {
        $fdb = $this->config->item('fingerdb');
        for ($x = 0, $y = count($fdb); $x < $y; $x++) {    
            unset($this->all_array);
            unset($config);
            $config = $this->_dbdefault;
            $config['dsn'] = 'mysql:host=' . $fdb[$x]['host'] . ';port=' . $fdb[$x]['port'] . ';dbname=' . $fdb[$x]['dbname'];
            $config['username'] = $fdb[$x]['username'];
            $config['password'] = $fdb[$x]['password'];
            $schoolname = $fdb[$x]['schoolname'];
            $db = $this->load->database($config, true);
            if ($locate == $fdb[$x]['dbname']) {    
                $db->select('*');
                $db->from('sentitems');
                $db->like('TextDecoded', $keyword);
                $db->like('DestinationNumber', $keyword2);
                $db->where("DATE_FORMAT(SendingDateTime, '%Y-%m-%d') >= ",$keyword3);
                $db->where("DATE_FORMAT(SendingDateTime, '%Y-%m-%d') <  ",$keyword4);
                $db->limit(10);
                $db->order_by('SendingDateTime', 'asc');
                $this->all_array = $db->get()->result();
                $db->close();
                return $this->all_array;
            }
        }
    }

    function autocomplete($title, $locate){
        $fdb = $this->config->item('fingerdb');
        for ($x = 0, $y = count($fdb); $x < $y; $x++) {    
            unset($config);
            $config = $this->_dbdefault;
            $config['dsn'] = 'mysql:host=' . $fdb[$x]['host'] . ';port=' . $fdb[$x]['port'] . ';dbname=' . $fdb[$x]['dbname'];
            $config['username'] = $fdb[$x]['username'];
            $config['password'] = $fdb[$x]['password'];
            $schoolname = $fdb[$x]['schoolname'];
            $db = $this->load->database($config, true);
            if ($locate == $fdb[$x]['dbname']) {    
                $db->like('TextDecoded', $title , 'both');
                $db->or_like('DestinationNumber', $title, 'both');
                $db->limit(10);
                return $db->get('sentitems')->result();
            }
        }
    }
}
