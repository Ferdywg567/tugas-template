<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Form_laporan extends CI_Controller {

    private $_dbdefault;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Laporan_model');
        $this->load->model('FilterLaporan_model');
        $this->load->library('datatables');
        $this->load->config('sekolah');
        $this->_dbdefault = array(
            'dbdriver' => 'pdo',
            'dbprefix' => '',
            'pconnect' => TRUE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );
    }

    public function index() {
        $fdb = $this->config->item('fingerdb');
        $array = array();

        for ($x = 0; $x < count($fdb);$x++) {
            $ss_db = $fdb[$x]['dbname'];
            array_push($array, $ss_db);
        }

        $data['database'] = $array;
        
        $this->template->load('template', 'laporan/selectdb', $data);
        $choose = $this->input->post('database');
        if ($choose == true) {
            redirect(base_url('form_laporan/view/'.$choose),'refresh');   
        }
        // echo '<pre>'; print_r($empty_arr [1]); echo '</pre>'; return;
    }

    public function view($data){
        $fdb = $this->config->item('fingerdb');
        $empty_arr = array();
        
        for ($x = 0, $y = count($fdb); $x < $y; $x++) {
            unset($config);
            unset($db);

            $config = $this->_dbdefault;

            $config['dsn'] = 'mysql:host=' . $fdb[$x]['host'] . ';port=' . $fdb[$x]['port'] . ';dbname=' . $fdb[$x]['dbname'];
            $config['username'] = $fdb[$x]['username'];
            $config['password'] = $fdb[$x]['password'];
            $schoolname = $fdb[$x]['schoolname'];

            $db = $this->load->database($config, true);
            if (!$db->conn_id) continue;

            $this->Laporan_model->SetDb($db);

            $ss_host = $fdb[$x]['host'];
            $ss_port = $fdb[$x]['port'];
            $ss_dbname = $fdb[$x]['dbname'];
            $ss_username = $fdb[$x]['username'];
            $ss_password = $fdb[$x]['password'];

            $laporan_data = $this->Laporan_model->get_laporan();
            array_push($empty_arr, $laporan_data);
            if ($data == $fdb[$x]['dbname']) {
                $query['data_laporan'] = $empty_arr[$x];
                $query['session'] = $fdb[$x]['dbname'];
                $this->load->view('laporan/query', $query);
            }
            $db->close();
        }
    }

    public function search($locate){
        $keyword3 = $this->input->post('keyword3');
        $keyword2 = $this->input->post('keyword2');
        $keyword = $this->input->post('keyword');
        $keyword4 = $this->input->post('keyword4');
        $data['filter'] = $this->FilterLaporan_model->get_filter_keyword($keyword, $keyword2, $keyword3, $keyword4, $locate);
        $data['session'] = $locate;
        $this->load->view('laporan/view', $data);   
    }

    function get_autocomplete($locate, $field){
        if ($field == 'pesan') {
            if (isset($_GET['term'])) {
                $result = $this->FilterLaporan_model->autocomplete($_GET['term'], $locate);
                if (count($result) > 0) {
                    foreach ($result as $row)
                        $arr_result[] = $row->TextDecoded;
                }
                echo json_encode($arr_result);
            }   
        }
        elseif ($field == 'nomer') {
            if (isset($_GET['term'])) {
                $result = $this->FilterLaporan_model->autocomplete($_GET['term'], $locate);
                if (count($result) > 0) {
                    foreach ($result as $row){
                        $arr_result[] = $row->DestinationNumber;

                        if ($arr_result == true) {
                            break;
                        }
                    }
                }
                echo json_encode($arr_result);
            }   
        }
    }

    public function backOption() {
        redirect(base_url(),'refresh');
    }

    public function cobaxls() {
        $numFormat = '0';
        $column = 0;

        // CONFIG UNTUK SETUP ARRAY SESUAI KOLOM-KOLOM YANG ADA DI MICROSOFT EXCEL
        $this->load->config('excel');
        $xlCol = $this->config->item('_xlsCol');

        // BUKA COMMENT DI BAWAH UNTUK MELIHAT
        // echo '<pre>'; print_r($xlCol); echo '</pre>'; return;





        /* -- CONTOH DATA STATIS , UNTUK DOWNLOAD EXCEL -- */

        //set the desired name of the excel file
        $fileName = 'Data-Form-Laporan';

        // Create new PHPExcel object
        $objPHPExcel = new PHPExcel();

        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $objPHPExcel->setActiveSheetIndex(0);

        //prepare the records to be added on the excel file in an array
        $excelData = array(
            "No Urut","Waktu Kirim", "Isi Pesan", "No Tujuan", "Status", "Sender ID"
        );
        $objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(22);
        $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(130);
        $objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(15);
        $objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(13);
        foreach ($excelData as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($column, 1, $field);
            $column++;
        }
        $id = $this->input->post('id');
        $sendingdatetime = $this->input->post('sendingdatetime');
        $textdecoded = $this->input->post('textdecoded');
        $destinationnumber = $this->input->post('destinationnumber');
        $status = $this->input->post('status');
        $senderid = $this->input->post('senderid');
        $full_array = [
            'id' => $id,
            'sendingdatetime' => $sendingdatetime,
            'textdecoded' => $textdecoded,
            'destinationnumber' => $destinationnumber,
            'status' => $status,
            'senderid' => $senderid,
        ];
        // echo "<pre>";
        // print_r($full_array['id']);
        // echo "</pre>"; return;
        $excel_row = 2;

        // foreach ($full_array as $value) {
        //     echo $value[0];
        // }return;

        for ($i = 0; $i < count($id); $i++) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $excel_row, $full_array['id'][$i]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $excel_row, $full_array['sendingdatetime'][$i]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $excel_row, $full_array['textdecoded'][$i]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $excel_row, $full_array['destinationnumber'][$i]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $excel_row, $full_array['status'][$i]);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $excel_row, $full_array['senderid'][$i]);
            $excel_row++;
        }
        // Set document properties
        $objPHPExcel->getProperties()->setCreator("Me")->setLastModifiedBy("Me")->setTitle("My Excel Sheet")->setSubject("My Excel Sheet")->setDescription("Excel Sheet")->setKeywords("Excel Sheet")->setCategory("Me");

        // Add column headers
        // $objPHPExcel->getActiveSheet()
        // ->setCellValue('A1', 'Last Name')
        // ->setCellValue('B1', 'First Name')
        // ->setCellValue('C1', 'Age')
        // ->setCellValue('D1', 'Sex')
        // ->setCellValue('E1', 'Location');

        //Put each record in a new cell
        // for($i=0; $i<count($excelData); $i++){
        //     $ii = $i+2;
        //     $objPHPExcel->getActiveSheet()->setCellValue('A'.$ii, $excelData[$i][0]);
        //     $objPHPExcel->getActiveSheet()->setCellValue('B'.$ii, $excelData[$i][1]);
        //     $objPHPExcel->getActiveSheet()->setCellValue('C'.$ii, $excelData[$i][2]);
        //     $objPHPExcel->getActiveSheet()->setCellValue('D'.$ii, $excelData[$i][3]);
        //     $objPHPExcel->getActiveSheet()->setCellValue('E'.$ii, $excelData[$i][4]);
        // }

        // Set worksheet title
        $objPHPExcel->getActiveSheet()->setTitle($fileName);

        // Redirect output to a client’s web browser (Excel2007)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="' . $fileName . '.xlsx"');
        header('Cache-Control: max-age=0');

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
    }
}
