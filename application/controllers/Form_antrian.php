<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Form_antrian extends CI_Controller {

    private $_dbdefault;

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Antrian_model');
        $this->load->model('FilterAntrian_model');
        $this->load->library('datatables');
        $this->load->config('sekolah');
        $this->_dbdefault = array(
            'dbdriver' => 'pdo',
            'dbprefix' => '',
            'pconnect' => TRUE,
            'db_debug' => TRUE,
            'cache_on' => FALSE,
            'cachedir' => '',
            'char_set' => 'utf8',
            'dbcollat' => 'utf8_general_ci',
            'swap_pre' => '',
            'encrypt' => FALSE,
            'compress' => FALSE,
            'stricton' => FALSE,
            'failover' => array(),
            'save_queries' => TRUE
        );
    }

    public function index() {

        $fdb = $this->config->item('fingerdb');
        $array = array();

        for ($x = 0; $x < count($fdb);$x++) {
            $ss_db = $fdb[$x]['dbname'];
            array_push($array, $ss_db);
        }

        $data['database'] = $array;

        $this->template->load('template', 'antrian/selectdb',$data);

        $choose = $this->input->post('database');
        
        if ($choose == true) {
            redirect(base_url('form_antrian/view/'.$choose),'refresh');   
        }
        // echo '<pre>'; print_r($empty_arr [1]); echo '</pre>'; return;
    }

    public function view($data){
        $fdb = $this->config->item('fingerdb');
        $empty_arr = array();
        
        for ($x = 0, $y = count($fdb); $x < $y; $x++) {
            unset($config);
            unset($db);

            $config = $this->_dbdefault;

            $config['dsn'] = 'mysql:host=' . $fdb[$x]['host'] . ';port=' . $fdb[$x]['port'] . ';dbname=' . $fdb[$x]['dbname'];
            $config['username'] = $fdb[$x]['username'];
            $config['password'] = $fdb[$x]['password'];
            $schoolname = $fdb[$x]['schoolname'];

            $db = $this->load->database($config, true);
            if (!$db->conn_id) continue;

            $this->Antrian_model->SetDb($db);

            $ss_host = $fdb[$x]['host'];
            $ss_port = $fdb[$x]['port'];
            $ss_dbname = $fdb[$x]['dbname'];
            $ss_username = $fdb[$x]['username'];
            $ss_password = $fdb[$x]['password'];

            $antrian_data = $this->Antrian_model->get_antrian();
            array_push($empty_arr, $antrian_data);
            if ($data == $fdb[$x]['dbname']) {
                $query['data_antrian'] = $empty_arr[$x];
                $query['session'] = $fdb[$x]['dbname'];
                $this->load->view('antrian/query', $query);
            }
            $db->close();
        }
    }
    public function search($locate){
        $keyword3 = $this->input->post('keyword3');
        $keyword2 = $this->input->post('keyword2');
        $keyword = $this->input->post('keyword');
        $data['filter'] = $this->FilterAntrian_model->get_filter_keyword($keyword, $keyword2, $locate);
        $data['session'] = $locate;
        $this->load->view('antrian/view', $data);
    }
    function get_autocomplete($locate, $field){
        if ($field == 'pesan') {
            if (isset($_GET['term'])) {
                $result = $this->FilterAntrian_model->autocomplete($_GET['term'], $locate);
                if (count($result) > 0) {
                    foreach ($result as $row)
                        $arr_result[] = $row->TextDecoded;
                }
                echo json_encode($arr_result);
            }   
        }
        elseif ($field == 'nomer') {
            if (isset($_GET['term'])) {
                $result = $this->FilterAntrian_model->autocomplete($_GET['term'], $locate);
                if (count($result) > 0) {
                    foreach ($result as $row){
                        $arr_result[] = $row->DestinationNumber;

                        if ($arr_result == true) {
                            break;
                        }
                    }
                }
                echo json_encode($arr_result);
            }   
        }
    }
    public function backOption() {
        redirect(base_url(),'refresh');
    }
}
