<meta http-equiv="refresh" content="300";charset="utf-8">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="msapplication-tap-highlight" content="no">
<meta name="description" content="Software sekolah dashboard absensi">
<meta name="keywords" content="materialize, admin template, dashboard template, flat admin template, responsive admin template,">
<!-- Favicons-->
<link rel="icon" href="<?php echo base_url() ?>template/img/favicon-32x32.png" sizes="32x32">
<!-- Favicons-->
<link rel="apple-touch-icon-precomposed" href="<?php echo base_url() ?>template/img/apple-touch-icon.png">
<!-- For iPhone -->
<meta name="msapplication-TileColor" content="#00bcd4">
<meta name="msapplication-TileImage" content="<?php echo base_url() ?>template/img/mstile-150x150.png">
<!-- For Windows Phone -->
<!-- CORE CSS-->
<link href="<?php echo base_url() ?>template/bootstrap/css/bootstrap.min.css" type="text/css" rel="stylesheet"
media="screen,projection">
<link href="<?php echo base_url() ?>template/bootstrap/css/starter-template.css" type="text/css" rel="stylesheet"
media="screen,projection">
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/jquery-ui.css') ?>">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/all.min.css">
<link rel="stylesheet" href="<?= base_url() ?>assets/css/animate.css">