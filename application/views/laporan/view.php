<!DOCTYPE html>
<html>
<head>
    <title>Dashboard - SMS</title>
    <?php $this->load->view('_partials/head.php'); ?>
</head>
<body>
    <?php $this->load->view('_partials/navbar.php'); ?>
    <div class="container">
        <form action="<?php echo base_url('form_laporan/search/' .$session) ?>" method="POST" class = "form-inline ">
            <!-- <input type="date" name="keyword"> -->
            <input type="text" name="keyword" placeholder="Isi Pesan" class="form-control mb-2 mr-sm-2" id="pesan">
            <input type="text" class="form-control mb-2 mr-sm-2" name="keyword2" placeholder="No Tujuan" id="nomer">
            <input type="text" name="keyword3" class="form-control mb-2 mr-sm-2" placeholder="yyyy/mm/dd" value="<?php echo date('Y-m-d') ?>">
            <input type="text" name="keyword4" class="form-control mb-2 mr-sm-2" placeholder="yyyy/mm/dd" value="<?php echo date('Y-m-d') ?>">
            <input type="submit" name="search_submit" value="cari" class="form-control mb-2 mr-sm-2">
        </form>
        <br>
        <!-- Isi Search -->
        <table class="table table-hover">
            <thead class = "thead-dark">
                <tr>
                    <th>No Urut</th>
                    <th>Waktu Kirim</th>
                    <th>Isi Pesan</th>
                    <th>No Tujuan</th>
                    <th>Status</th>
                    <th>Sender ID</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($filter as $filters) : ?>
                    <tr>
                        <td><?php echo $filters->ID ?></td>
                        <td><?php echo $filters->SendingDateTime ?></td>
                        <td><?php echo $filters->TextDecoded ?></td>
                        <td><?php echo $filters->DestinationNumber ?></td>
                        <td><?php echo $filters->Status ?></td>
                        <td><?php echo $filters->SenderID ?></td>
                    </tr>
                <?php endforeach ?>
            </tbody>
        </table>
        <form action="<?= base_url('form_laporan/cobaxls') ?>" method = "POST">
            <?php foreach ($filter as $value => $key):?>
                <input type="hidden" name="<?php echo 'id['.$value.']'  ?>" value="<?php echo $key->ID ?>">
                <input type="hidden" name="<?php echo 'sendingdatetime['.$value.']'  ?>" value="<?php echo $key->SendingDateTime ?>">
                <input type="hidden" name="<?php echo 'textdecoded['.$value.']'  ?>" value="<?php echo $key->TextDecoded ?>">
                <input type="hidden" name="<?php echo 'destinationnumber['.$value.']'  ?>" value="<?php echo $key->DestinationNumber ?>">
                <input type="hidden" name="<?php echo 'status['.$value.']'  ?>" value="<?php echo $key->Status ?>">
                <input type="hidden" name="<?php echo 'senderid['.$value.']'  ?>" value="<?php echo $key->SenderID ?>">
            <?php endforeach ?>
            <input type="submit" name="search_submit" value="Download" class="form-control mb-2 mr-sm-2 btn-success">
        </form>
    </div>

<?php $this->load->view('_partials/script.php'); ?>

    <script type="text/javascript">
        $(document).ready(function(){

            $('#pesan').autocomplete({
                source: "<?php echo site_url('form_laporan/get_autocomplete/' . "$session" . '/pesan');?>",
            });

            $('#nomer').autocomplete({
                source: "<?php echo site_url('form_laporan/get_autocomplete/' . "$session" . '/nomer');?>",
            });

        });
    </script>

</body>
</html>